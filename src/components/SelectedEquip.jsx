import React from "react";

import Titulars from "./Titulars";
import Supplents from "./Supplents";

const SelectedEquip = () => {
  return (
    <section>
      <Titulars />
      <Supplents />
    </section>
  );
};

export default SelectedEquip;
