import React from "react";
import { connect } from "react-redux";

const Supplents = ({ supplents, removeSupplent }) => {
  return (
    <section>
      <h2>Supplents</h2>
      <div className="supplents">
        {supplents.map((supplent, index) => {
          return (
            <article className="supplent" key={index}>
              <div>
                <img src={supplent.picture} alt={supplent.name} />
                <button onClick={() => removeSupplent(supplent)}>X</button>
              </div>
              <p>{supplent.name}</p>
            </article>
          );
        })}
      </div>
    </section>
  );
};

const mapStateToProps = (state) => ({
  supplents: state.supplents,
});

const mapDispatchToProps = (dispatch) => ({
  removeSupplent(player) {
    dispatch({
      type: "REMOVE_SUPPLENT",
      player,
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Supplents);
