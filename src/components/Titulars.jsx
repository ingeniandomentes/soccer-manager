import React from "react";
import { connect } from "react-redux";
import cancha from "../cancha.svg";
const Titulars = ({ titulars, removeTitular }) => {
  return (
    <section>
      <h2>Titulars</h2>
      <div className="field">
        {titulars.map((titular, index) => {
          return (
            <article className="titular" key={index}>
              <div>
                <img src={titular.picture} alt={titular.name} />
                <button onClick={() => removeTitular(titular)}>X</button>
              </div>
              <p>{titular.name}</p>
            </article>
          );
        })}
        <img src={cancha} alt="Soccer field" />
      </div>
    </section>
  );
};

const mapStateToProps = (state) => ({
  titulars: state.titulars,
});

const mapDispatchToProps = (dispatch) => ({
  removeTitular(player) {
    dispatch({
      type: "REMOVE_TITULAR",
      player,
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Titulars);
