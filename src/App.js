import React from "react";
import { Provider } from "react-redux";

import store from "./store";

import Players from "./components/Players";
import SelectedEquip from "./components/SelectedEquip";

import "./styles/styles.scss";

const App = () => {
  return (
    <Provider store={store}>
      <main>
        <h1>Soccer Manager</h1>
        <Players />
        <SelectedEquip />
      </main>
    </Provider>
  );
};

export default App;
